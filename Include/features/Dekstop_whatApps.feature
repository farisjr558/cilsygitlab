#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: enable run whatsApp on dekstop base.

  @TC001 @login @dekstop @regression
  Scenario: user want link they whatsApp phone, to whatsApp dekstop
    Given apps already opened
    And apps display QR code
    And user already standby for scan whatsApp from android
    When user scan QR code to dekstop apps
    Then app direct to loading page for loggin whatsApp
    And app loggined already

  @TC002 @newChat @dekstop @regression
  Scenario: user do new chat by button plus for newChat
    Given user click nav menu new chat
    When user fill search name
      | name |
      | kiki |
    And user select name from result
    Then user can start convertation message
    And user can close result search

  @TC003 @convertationPanel @dekstop @regression
  Scenario: user can sent message from text area convertation panel
    Given user fill message
      | message |
      | Hello   |
    When user click sent
    Then convertation will display text message

  @TC004 @convertationPanel @sentText @dekstop @regression
  Scenario: user can sent image file
    Given user click icon cliping
    And user select image
      | fileImage |
      | image.jpg |
    When user submit file
    And user click sent
    Then convertation will display image message

  @TC005 @convertationPanel @sentImage @dekstop @regression
  Scenario: user can sent image file
    Given user click icon cliping
    And user select image
      | fileImage |
      | image.jpg |
    When user submit file
    And user click sent
    Then convertation will display image message

  @TC006 @convertationPanel @deleteMessage @dekstop @regression
  Scenario: user delete text message has been sent
    Given system display convertation panel
    And user ahve history chat
    When user click arrow at message out text
    And system display option action
      | action       |
      | delelet chat |
    And system display confirm allert message
    And user select yes
    Then system will notice history text has been deleted

  @TC007 @convertationPanel @groupChat @createGroup @dekstop @regression
  Scenario: user add new group
    Given user click three dot at nav menu
    When user select add new group
    And user add some member
      | member   |
      | member A |
      | member B |
    And user go to next for fill title
      | title    |
      | newGroup |
    And do submit by click checklis icon
    Then system display convertation pannel new group

  @TC008 @convertationPanel @groupChat @addMember @dekstop @regression
  Scenario: user add member at group
    Given user go to detail member
    And user click button add new member
    When user select member want to add
      | member   |
      | member A |
    And user submit to add member
    Then system will display member has been added

  @TC009 @convertationPanel @groupChat @removeMember @dekstop @regression
  Scenario: user remove member at group
    Given user go to detail member
    When user select member want to remove
      | member   |
      | member A |
    And user submit to remove member
    Then system will not display member has been remove

  @TC010 @convertationPanel @groupChat @leaveGroup @dekstop @regression
  Scenario: user leave at group
    Given user go to detail member
    When click button leave group
    And user submit to leave group
    Then system display popUp succes leave group
