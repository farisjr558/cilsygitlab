import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class stepDefWA {
	@Given("apps already opened")
public void apps_already_opened() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("apps display QR code")
public void apps_display_QR_code() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user already standby for scan whatsApp from android")
public void user_already_standby_for_scan_whatsApp_from_android() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user scan QR code to dekstop apps")
public void user_scan_QR_code_to_dekstop_apps() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("app direct to loading page for loggin whatsApp")
public void app_direct_to_loading_page_for_loggin_whatsApp() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("app loggined already")
public void app_loggined_already() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user click nav menu new chat")
public void user_click_nav_menu_new_chat() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user fill search name")
public void user_fill_search_name() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user select name from result")
public void user_select_name_from_result() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("user can start convertation message")
public void user_can_start_convertation_message() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("user can close result search")
public void user_can_close_result_search() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user fill message")
public void user_fill_message() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user click sent")
public void user_click_sent() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("convertation will display text message")
public void convertation_will_display_text_message() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user click icon cliping")
public void user_click_icon_cliping() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user select image")
public void user_select_image() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user submit file")
public void user_submit_file() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("convertation will display image message")
public void convertation_will_display_image_message() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("system display convertation panel")
public void system_display_convertation_panel() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user ahve history chat")
public void user_ahve_history_chat() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user click arrow at message out text")
public void user_click_arrow_at_message_out_text() {
    // Write code here that turns the phrase above into concrete actions

}

@When("system display option action")
public void system_display_option_action() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("system display confirm allert message")
public void system_display_confirm_allert_message() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user select yes")
public void user_select_yes() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("system will notice history text has been deleted")
public void system_will_notice_history_text_has_been_deleted() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user click three dot at nav menu")
public void user_click_three_dot_at_nav_menu() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user select add new group")
public void user_select_add_new_group() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user add some member")
public void user_add_some_member() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user go to next for fill title")
public void user_go_to_next_for_fill_title() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("do submit by click checklis icon")
public void do_submit_by_click_checklis_icon() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("system display convertation pannel new group")
public void system_display_convertation_pannel_new_group() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user go to detail member")
public void user_go_to_detail_member() {
    // Write code here that turns the phrase above into concrete actions

}

@Given("user click button add new member")
public void user_click_button_add_new_member() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user select member want to add")
public void user_select_member_want_to_add() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user submit to add member")
public void user_submit_to_add_member() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("system will display member has been added")
public void system_will_display_member_has_been_added() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user select member want to remove")
public void user_select_member_want_to_remove() {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.

}

@When("user submit to remove member")
public void user_submit_to_remove_member() {
    // Write code here that turns the phrase above into concrete actions
//
}

@Then("system will not display member has been remove")
public void system_will_not_display_member_has_been_remove() {
    // Write code here that turns the phrase above into concrete actions

}

@When("click button leave group")
public void click_button_leave_group() {
    // Write code here that turns the phrase above into concrete actions

}

@When("user submit to leave group")
public void user_submit_to_leave_group() {
    // Write code here that turns the phrase above into concrete actions

}

@Then("system display popUp succes leave group")
public void system_display_popUp_succes_leave_group() {
    // Write code here that turns the phrase above into concrete actions

}
}