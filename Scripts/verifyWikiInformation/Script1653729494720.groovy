import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import java.util.Date as Date
import java.text.SimpleDateFormat as SimpleDateFormat
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://spacex.land/')

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('spaceX/homePage/textBtn_launchDiscovery'))

WebUI.waitForElementVisible(findTestObject('spaceX/launchesPage/pagination_launches'), 0)

//---SetCountTable---------
rows_count_outlet = tutorialCheckCount('sc-fzozJi dteCCc')

println(rows_count_outlet)

//---ActionLoop---------
for (i = 1; i <= rows_count_outlet; i++) {
    pickSection = findTestObject('spaceX/launchesPage/textBtn_dictionaryLaunches', [('index') : i])

    WebUI.click(pickSection)

    WebUI.waitForElementPresent(findTestObject('spaceX/launchesPage/detailLaunchesPage/dateOfLaunches'), 111)

    String missionName = WebUI.getText(findTestObject('spaceX/launchesPage/detailLaunchesPage/missionName')).substring(3)

    String sDate = WebUI.getText(findTestObject('spaceX/launchesPage/detailLaunchesPage/dateOfLaunches')).substring(0, 10)

    String rocketName = WebUI.getText(findTestObject('spaceX/launchesPage/detailLaunchesPage/dateOfLaunches')).substring(
        14)

    println(missionName)

    println(sDate)

    println(rocketName)

// convert date to english date
    KeywordLogger log = new KeywordLogger()

    SimpleDateFormat sdf = new SimpleDateFormat('MM/dd/yyyy')

    Date parsedDate = null

    String sentDate = sDate

    parsedDate = sdf.parse(sentDate)

    SimpleDateFormat print = new SimpleDateFormat('dd MMMM yyyy', Locale.ENGLISH)

// add "," becouse format wikipedia launch date is with ,
    String dateLaunch = print.format(parsedDate) + ','

    println(dateLaunch)


	
//wikipedia Page
    WebUI.navigateToUrl('https://wikipedia.com/')

    WebUI.setText(findTestObject('Object Repository/wikiPedia/homePage/searchField'), missionName)

    WebUI.sendKeys(findTestObject('Object Repository/wikiPedia/homePage/searchField'), Keys.chord(Keys.ENTER))
	
	dateValid = findTestObject('Object Repository/wikiPedia/resultSearch/infoData', ['component' : dateLaunch])

	if (WebUI.verifyElementPresent(dateValid, 0, FailureHandling.OPTIONAL)) {	
		println(dateLaunch + 'found')
    } else {
        println(dateLaunch + 'not found')
    }
    
    WebUI.openBrowser('https://spacex.land/')

    WebUI.maximizeWindow()

    WebUI.delay(2)

    WebUI.click(findTestObject('spaceX/homePage/textBtn_launchDiscovery'))

    WebUI.waitForElementVisible(findTestObject('spaceX/launchesPage/pagination_launches'), 0) //	WebUI.click(findTestObject('spaceX/launchesPage/detailLaunchesPage/btn_back'))
}



def tutorialCheckCount(def XXX) {
    WebDriver driver = DriverFactory.getWebDriver()

    WebElement Table = driver.findElement(By.xpath(('//*[@class=\'' + XXX) + '\']'))

    List<WebElement> rows_table = Table.findElements(By.tagName('div'))

    println(Table)

    println(rows_table)

    return rows_table.size()
}

